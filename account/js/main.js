
/* SETTINGS */

var validate = false;

/* DOCUMENT LOAD */

$(function(){

    __initForm();

    $("#accountForm").on('submit', function(e) {

        if ( !validate || $(this).valid() ){

            resetMessages();
            e.preventDefault();

            let formData = createFormData();

            manageRequest(formData);

        }
        
    });
    
    // START - JQuery Form Validation

    if (validate){

        $("#accountForm").validate({

            errorClass : "formError",
            onkeyup: false,
            focusCleanup: true,

            rules : {
                "name":{
                    minlength: 2,
                    required: true
                },
                "email":{
                    minlength: 2,
                    required: true
                },
                "password" : {
                    minlength : 5,
                    required: true
                }
            },

            messages: {

                errorClass : "formError",
                onkeyup: false,
                focusCleanup: true,

                "name": {
                    required: "We need your name",
                    minlength: "This name seems too short"
                },
                "email": {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com",
                    minlength: "This email seems too short"
                }
            },

        });

    }
    
    // END - JQuery Form Validation

});

// START - Manage and send AJAX call

function manageRequest(formData){

    let xhr = new XMLHttpRequest();

    xhr.open("POST", "../core/verifyForm.php");

    xhr.responseType = 'json';

    xhr.onreadystatechange = function() {

        if (xhr.readyState == XMLHttpRequest.DONE) {

            let response = xhr.response;
            
            if (response.success == false){

                let errors = JSON.parse(response.errors);

                for (let i = 0; i < errors.length; i++){

                    let error = errors[i];

                    console.log(error);

                    let message = getError(error['error'], 'es');
                    let field = error['field'];

                    showError(message, field);

                }

            } else {

                console.log(response);

            }
        }
    }

    xhr.send(formData);

}

// END - Manage and send AJAX call