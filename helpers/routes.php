<?php

interface routes{

    const root = realpath($_SERVER["DOCUMENT_ROOT"]);
    const project = 'protohome';
    
    const directory = $root.'/'.$project.'/';

    public function get($file){

        switch ($file){

            case 'connect':

                return $directory.'/'.'core'.'/'.'Connect.php';

            break;

            case 'formvalidation':

                return $directory.'/'.'core'.'/'.'FormValidation.php';

            break;

            case 'form':

                return $directory.'/'.'model'.'/'.'Form.php';

            break;

            case 'formerror':

                return $directory.'/'.'model'.'/'.'FormError.php';

            break;

            case 'user':

                return $directory.'/'.'model'.'/'.'User.php';

            break;

        }

    }

}

?>