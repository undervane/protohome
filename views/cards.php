
<!-- Section Title -->

<div class="container-fluid">
    <div class="row">

        <div class="offset-lg-1">
            <h2 class="section-title">Alojamientos</h2>
        </div>

    </div>
</div>

<!-- END Section Title -->

<!-- Cards Section -->

<div class="container-fluid">
    <div class="row justify-content-center">

    <!-- CARD -->

        <div class="card" style="width: 18rem;">

        <img class="card-img-top" src="" alt="Card image cap">

        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text"></p>
            <a href="#" class="btn btn-danger">Ver Más</a>
        </div>

        </div>

    <!-- CARD -->

        <div class="card col-lg-3" style="width: 18rem;">

            <img class="card-img-top" src="img/cards/card1.jpg" alt="Card image cap">

            <div class="card-body">
                <h5 class="card-title">Casa Rústica</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>

        <!-- CARD -->

        <div class="card col-lg-3 modalEdit modalAnimating" style="width: 18rem;">

            <img class="card-img-top" src="img/cards/card1.jpg" alt="Card image cap">

            <div class="card-body">
                <h5 class="card-title">Casa Rústica</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>

        <!-- CARD -->

        <div class="card  col-lg-3" style="width: 18rem;">

            <img class="card-img-top" src="img/cards/card2.jpg" alt="Card image cap">

            <div class="card-body">
                <h5 class="card-title">Piso en Palma</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>

        <!-- CARD -->

        <div class="card  col-lg-3" style="width: 18rem;">
            
            <img class="card-img-top" src="img/cards/card3.jpg" alt="Card image cap">
            
            <div class="card-body">
                <h5 class="card-title">Ático Moderno</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>

        <!-- CARD -->

        <div class="card  col-lg-3" style="width: 18rem;">

            <img class="card-img-top" src="img/cards/card4.jpg" alt="Card image cap">

            <div class="card-body">
                <h5 class="card-title">Casa en el Bosque</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>

        <!-- CARD -->

        <div class="card  col-lg-3" style="width: 18rem;">

            <img class="card-img-top" src="img/cards/card5.jpg" alt="Card image cap">

            <div class="card-body">
                <h5 class="card-title">Casa Mallorquina</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>

        <!-- CARD -->

        <div class="card  col-lg-3" style="width: 18rem;">

            <img class="card-img-top" src="img/cards/card6.jpg" alt="Card image cap">

            <div class="card-body">
                <h5 class="card-title">Casa en la Playa</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-danger">Ver Más</a>
            </div>

        </div>
    </div>
</div>

<!-- END Card Section -->