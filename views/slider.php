
<!-- Slider -->

<div id="carouselExampleSlidesOnly" class="carousel slide d-lg-block" data-ride="carousel">
    <div class="overlay vertical-center">
        <div class="container">

            <div class="row title">
                <h1>Reserva alojamientos únicos y experiencias en todo el mundo</h1>
            </div>

            <div class="row search-box shadow w-90">
                <div class="row w-100">

                    <div class="col-2 col-md-1 align-self-center">
                        <img class="search-box-form-img" src="img/search.svg">
                    </div>

                    <div class="col-9">
                        <form class="search-box-form">
                            <input type="text" placeholder="Prueba con Francia">
                        </form>
                    </div>

                    <div class="d-none d-md-block col-md-2 search-btn">
                        <button>Buscar</button>
                    </div>

                </div>
            </div>
            
            <div class="row d-md-none d-sm-block w-90 search-btn">
                        <button class="shadow">Buscar</button>
            </div>
            
        </div>
    </div>

    <div class="carousel-inner">

        <div class="carousel-item active">
            <picture>
                <source class="d-block w-100" srcset="img/slider/slide1@x2.jpg" media="(min-width: 720px)" />
                <img class="d-block w-100" srcset="img/slider/slide1.jpg" alt="First slide" />
            </picture>
        </div>

        <div class="carousel-item">
            <picture>
                <source class="d-block w-100" srcset="img/slider/slide2@x2.jpg" media="(min-width: 720px)" />
                <img class="d-block w-100" srcset="img/slider/slide2.jpg" alt="Second slide" />
            </picture>                
        </div>

        <div class="carousel-item">
            <picture>
                <source class="d-block w-100" srcset="img/slider/slide3@x2.jpg" media="(min-width: 720px)" />
                <img class="d-block w-100" srcset="img/slider/slide3.jpg" alt="Third slide" />
            </picture>
        </div>
        
    </div>
</div>

<!-- END Slider -->