<div id="modal" class="hide">

    <div id="img-modal" class="hide">

        <div class="drop-container">
            <label for="img">Upload your image</label>
            <input type="file" class="dropify" id="drop-img" name="img" data-height="300"/>
        </div>

    </div>

    <div id="editor-modal" class="hide">

        <div class="editor-container">
            <textarea id="text" name="editor"></textarea>
        </div>

    </div>

    <div class="container-fluid">

        <form>

            <div class="img-container">
                <img id="image" src="img/placeholder.png"/>
            </div>

            <div class="articles-container row">

                <div class="offset-md-4 col-md-4 text-center">

                <input type="text" name="left-title" data="Left" size="7" value="Left Title" maxlength="20" />
                <div class="text-div" data="left">

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In schola desinis. Duo Reges: constructio interrete. Summae mihi videtur inscitiae. </p>
                    
                    <p>Disserendi artem nullam habuit. <b>Atqui reperies, inquit, in hoc quidem pertinacem;</b> Gerendus est mos, modo recte sentiat. Sed residamus, inquit, si placet. </p>
                    
                    </div>

                </div>

            </div>

        </form>

    </div>

</div>