<!-- Footer -->

<div class="container-fluid footer">
    <div class="row">
        <div class="offset-md-2 col-sm-12 col-md-2">

            <ul class="footer-col">
                <li class="sub-heading">Discover</li>
                <a href="#"><li class="body">Roma</li></a>
                <a href="#"><li class="body">Paris</li></a>
                <a href="#"><li class="body">Toronto</li></a>
            </ul>

        </div>

        <div class="col-sm-12 col-md-2">

            <ul class="footer-col">
                <li class="sub-heading">Explore</li>
                <a href="#"><li class="body">Dublín</li></a>
                <a href="#"><li class="body">Casablanca</li></a>
                <a href="#"><li class="body">Estrasburgo</li></a>
            </ul>
            
        </div>

        <div class="col-sm-12 col-md-2">

            <ul class="footer-col">
                <li class="sub-heading">Legal</li>
                <a href="#"><li class="body">Terms</li></a>
                <a href="#"><li class="body">Privacy</li></a>
                <a href="#"><li class="body">Cookies</li></a>
            </ul>

        </div>

        <div class="col-sm-12 col-md-2">

            <ul>
                <li class="sub-heading">Social</li>
                <a href="#"><li class="body"><i class="fab fa-twitter"></i></li></a>
                <a href="#"><li class="body"><i class="fab fa-pinterest"></i></li></a>
                <a href="#"><li class="body"><i class="fab fa-youtube"></i></li></a>
            </ul>

        </div>
    </div>
</div>

<!-- END Footer -->