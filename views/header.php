
 <!-- Nav Bar -->

<div class="container-fluid">
    <div class="row">
        <div class="col-12 no-padding">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">

                <a class="navbar-brand" href="#">
                    <img src="img/logo.svg" class="d-inline-block align-top svg logo" alt="">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="#">Ayuda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="new/">Nueva propiedad</a>
                        </li>
                        <li class="nav-item hide" id="nav-register">
                            <a class="nav-link" href="account/index.php?register">Regístrate</a>
                            <div class="popup">
                                    <div class="arrow"></div>
                                    <div class="login-popup">

                                        <form>
                                            <fieldset>
                                                <label for="email">Email</label>
                                                <input type="text" name="email">
                                            </fieldset>
                                            
                                            <fieldset>
                                                    <label for="password">Password</label>
                                                    <input type="password" name="password">
                                            </fieldset>

                                            <button>Login</button>

                                        </form>
                                        
                                    </div>
                                </div>
                            
                        </li>
                        <li class="nav-item hide" id="login">
                            
                            <a class="nav-link" href="account/index.php?login">Iniciar Sesión</a>
                            <div class="popup">
                                <div class="arrow"></div>
                                <div class="login-popup">

                                    <form>
                                        <fieldset>
                                            <label for="email">Email</label>
                                            <input type="text" name="email">
                                        </fieldset>
                                        
                                        <fieldset>
                                                <label for="password">Contraseña</label>
                                                <input type="password" name="password">
                                        </fieldset>

                                        <button>Login</button>

                                    </form>
                                    
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="account">Mi Cuenta</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>

<!-- END Nav Bar -->