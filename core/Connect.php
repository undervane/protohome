<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/protohome/vendor/autoload.php';

class Connect{

  // define a variable for the database connection
  private $_pdo;
  public $_fpdo;

  // when an instance of 'connection' class is created a connection is made through mysqli
  public function __construct() {

    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    $db_config = require_once $root.'/protohome/config/database.php';
    
    $driver = $db_config["driver"];
    $host = $db_config["host"];
    $user = $db_config["user"];
    $pass = $db_config["pass"];
    $database = $db_config["database"];
    $charset = $db_config["charset"];

    $options = array(  
      PDO::ATTR_PERSISTENT => true,  
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION  
    );  

    try{

      $this->_pdo = new PDO($driver.":dbname=".$database, $user, $pass, $options);
      $this->_fpdo = new FluentPDO($this->_pdo);

    } catch(PDOException $e){  

      $this->error = $e->getMessage();  

    } 

  }

  // method used to send a query to database
  public function query($sql){

    $statement = $this->_pdo->prepare($sql);
    $statement->execute();
  
  }

}