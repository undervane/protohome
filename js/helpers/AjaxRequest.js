
class AjaxRequest extends XMLHttpRequest{

    constructor() {

        super();
        this.responseType = 'json';
 
    }

    post(post, to){

        this.open("POST", to);
        this.action = post;
        
    }

    success(callback){

        this.onreadystatechange = function() {

            if (this.readyState == XMLHttpRequest.DONE) {

                if (this.status == 200){ callback(); }
                
            }
        }

        this.send(this.action);
    }

    // NEEDS FIXING FAIL OVERWRITING ONREADYSTATECHANGE

    // fail(callback){

    //     this.onreadystatechange = function() {

    //         if (this.readyState == XMLHttpRequest.DONE) {

    //             if (this.status != 200){callback();}

    //         }
    //     }
    // }
}