<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/protohome/model/User.php';
require_once $root.'/protohome/controller/FormValidation.php';

class UserController extends User{

    protected $fields;

    public function __construct($POST){

        parent::__construct();

        $form = new FormValidation(['name', 'email', 'password']);

        $fields[] = [$POST['name'], [['required'],['max-length', '10']]];
        $fields[] = [$POST['email'],[['required'],['email']]];
        $fields[] = [$POST['password'],[['required'],['min-length', '3']]];

        $form->add($fields);
        
        try{

            $form->validate();

            $this->_name = $POST['name'];
            $this->_email = $POST['email'];
            $this->_password = password_hash($POST['password'], PASSWORD_BCRYPT);

            $this->insert();

        } catch (Exception $errors) {

            throw $errors;

        }
    }
}

?>
