<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/protohome/model/Property.php';
require_once $root.'/protohome/controller/FormValidation.php';
require_once $root.'/protohome/model/Image.php';

// include composer autoload
require '../vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;

class PropertyController extends Property{

    protected $fields;

    public function __construct($POST){

        parent::__construct();

        if ($POST != null){

            $form = new FormValidation(['img', 'title', 'country', 'body']);

            $fields[] = [$_FILES['img'],[['required'], ['max-size', 1000], ['type', ['jpg', 'png']]]];
            $fields[] = [$POST['title'],[['required'],['min-length', 4],['max-length', 20]]];
            $fields[] = [$POST['country'],[['required']]];
            $fields[] = [$POST['body'],[['required'],['min-length', 10]]];

            $form->add($fields);
            
            try{

                $form->validate();

                $image = new Image($_FILES["img"]);
                
                $this->_img = $image;
                $this->_title = $POST['title'];
                $this->_country = $POST['country'];
                $this->_body = $POST['body'];

                $this->insert();

            } catch (Exception $errors) {

                throw $errors;

            }
        }
    }
}

?>
