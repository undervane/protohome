
<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/validation.php/model/Form.php';
require_once $root.'/validation.php/model/FormError.php';

class FormValidation extends Form{

    protected $_errors;
    protected $_loop;

    public function validate(){

        $this->_loop = 0;
        $this->_errors = null;

        foreach ($this->_fields as &$field){

            $content = array_key_exists(0, $field) ? $field[0] : null;
            $rules = $field[1];
            
            try { $this->isValid($content, $rules); }
            
            catch (Exception $e) {

                $message = $e->getMessage();
                $field = $this->_structure[$this->_loop];

                $this->_errors[] = new FormError($message, $field);

            }

            $this->_loop++;
        }

        if ($this->_errors != NULL){

            throw new Exception(json_encode($this->_errors));

        }

    }

    protected function isValid($content, $rules){

        foreach ($rules as &$rule){

            $condition = $rule[0];
            $detail = array_key_exists(1, $rule) ? $rule[1] : false;

            switch($condition){

                case 'required':

                    if ($content == '' || $content == null){ throw new Exception('required'); }

                break;

                case 'min-length':

                    if (strlen($content) < $detail){ throw new Exception('min-length'); }

                break;

                case 'max-length':

                    if (strlen($content) > $detail){ throw new Exception('max-length'); }

                break;

                case 'email':

                    if (strpos($content, '@') == NULL || strpos($content, '.') == NULL){ throw new Exception('email'); }

                break;

                case 'type':

                    if ($this->verifyFile($content['tmp_name'], $detail)){ throw new Exception('invalid'); }

                break;

                case 'max-size':

                    $bytes = $detail * 1024;

                    if ($content['size'] > $bytes){ throw new Exception('max-size'); }

                break;

                // Add here new cases containing new rules
                
                default: throw new Exception(`Undefined rule: $rule`);

            }
        }
    }

    private function verifyFile($FILE, $types) {
        
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mtype = finfo_file($finfo, $FILE);
        finfo_close($finfo);

        foreach ($types as &$type){

            $mimes = $this->getMimes($type);

            foreach ($mimes as &$mime){

                if ($mtype == $mime){ return false; }

            }
        }

        return true;

    }

    private function getMimes($type){

        switch ($type){

            case 'png':

            return ['image/png', 'application/png', 'application/x-png'];

            case 'jpeg' || 'jpg':

            return ['image/jpeg', 'image/jpg', 'image/jpe_', 'image/pjpeg', 'image/vnd.swiftview-jpeg', 'image/jp_', 'application/jpg', 'application/x-jpg', 'image/pipeg', 'image/x-xbitmap'];

            // Add here more mime types verification

        }
    }
}

?>
