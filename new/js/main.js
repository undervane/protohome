
var reference;

$(function(){

    $('.dropify').dropify();

    CKEDITOR.replace( "editor" );

    $('.js-example-basic-single').select2();

    CKEDITOR.instances.text.setData('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In schola desinis. Duo Reges: constructio interrete. Summae mihi videtur inscitiae. </p><p>Disserendi artem nullam habuit. <b>Atqui reperies, inquit, in hoc quidem pertinacem;</b> Gerendus est mos, modo recte sentiat. Sed residamus, inquit, si placet. </p>');

    $(document).on('change', '#drop-img', function(e){

        var tgt = e.target || window.event.srcElement,
        files = tgt.files;

        // Parse image

        var reader = new FileReader();

        reader.onload = function () {
            $('#image').attr('src', reader.result);
        };

        reader.readAsDataURL(files[0]);

    });

    $(document).on('click', '.submit', function(){

        let data = $(this).attr('data');

        let title = $('#title').val();

        let editor = CKEDITOR.instances.text.getData();

        if(title != '' && editor != ''){

        switch (data){

            case "left": 

                $('#left-title').html(title);
                $('#left-div').html(editor);

                break;

            case "right": 

                $('#right-title').html(title);
                $('#right-div').html(editor);

                break;

            default: console.warn(`No case ${data}`);

        }

        $('#title').val('');
        CKEDITOR.instances.text.setData(''); 

    } else { alert('You have to complete all fields'); }

    });

    $(document).on('submit', '#form', function(e){

        e.preventDefault();

    });

    $(document).on('click', '#modal', function(e){

        if ($(e.target).is('#modal') && $(this).hasClass('hide') != true){

            // If modal is visible

            $(this).addClass('hide');
            $('#img-modal').addClass('hide');
            $('#editor-modal').addClass('hide');

            if (reference != undefined){

                let text = CKEDITOR.instances.text.getData();

                console.log(text);

                if (text.length != 0){

                    reference.html(text);

                } else {

                    reference.html('Untitled Text');

                }

            }

            notifyChange();

        }

    });

    $(document).on('click', '#image', function(){

        $('#modal').removeClass('hide');
        $('#img-modal').removeClass('hide');

    });

    $(document).on('keydown', 'input', function(){

        let length = $(this).val().length;

        $(this).attr('size', length);

        if (length == 0){

            $(this).attr('size', 7);

        }


    });

    $(document).on('focusout', '.title', function(){

        let length = $(this).val().length;

        $(this).attr('size', length);

        if (length == 0){

            $(this).attr('size', 5);
            $(this).val('Title');

        }

        notifyChange();

    });

    $(document).on('click', '.text-div', function(e){

        $('#modal').removeClass('hide');
        $('#editor-modal').removeClass('hide');

        CKEDITOR.instances.text.setData($(this).html());

        reference = $(this);

    });

    $(document).on('click', '#status', function(){

        if ($(this).hasClass('action')){

            updateData();
            $(this).toggle('action').toggle('pending');

        }

    });

});

function notifyChange(){

    if (!$('#status').hasClass('pending')){

        $('#status').html('Save').addClass('action');
    
    }

}

function updateData(){

    let formData = new FormData();

    console.log($('input[name=title]').val());

    let body = CKEDITOR.instances.text.document.getBody().getText();
    
    formData.append("img", $('input[name=img]').prop('files')[0]);
    formData.append("title", $('input[name=title]').val());
    formData.append("country", $('select[name=country]').val());
    formData.append("body", body);

    let ajax = new AjaxRequest;

    ajax.post(formData, "../core/verifyProperty.php");

    ajax.success(() => {

        let response = ajax.response;

        if (response.success){

            $('#status').html('Saved').removeClass('pending');
    
            setTimeout(function(){

                $('#status').html('');

            }, 1500);
    
        } else {
    
            let errors = JSON.parse(response.errors);
    
            for (let i = 0; i < errors.length; i++){
    
                let error = errors[i];
    
                // let message = getError(error['error'], 'es');
                let field = error['field'];
    
                // showError(message, field);
    
                $('#status').html('Error: ' + error['error'] + '<br>' + 'Field: ' + field ).removeClass('pending');
    
            }
    
        }
    });
    
    // ajax.fail(function(){

    //     console.error(`AJAX failed with status ${ajax.status}`);

    // });
}


