<?php

    return [

        "web" => [

            "desktop" => [null, 900],
            "mobile" => [600, null]

        ],
        "ios" => [

            "iphone" => [700, null],
            "ipad-pro" => [null, 600]

        ],
        "thumbnails" => [

            "extra-tiny" => [null, 20],
            "tiny" => [100, null]

        ]

    ]

?>