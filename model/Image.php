<?php

require '../vendor/autoload.php';

use Intervention\Image\ImageManager;

class Image{

    private $_original;
    private $_path;
    private $_sizes;
    private $_manager;

    public function __construct($FILE){

        $this->_manager = new ImageManager(array('driver' => 'gd'));

        // PATHS MAY BE IMPROVED

        if (!file_exists(getenv('HOME')."/"."uploads/")) {
            mkdir(getenv('HOME')."/"."uploads/", 0777, true);
        }
        $uploads_dir = getenv('HOME')."/"."uploads/";
        $tmp_name = $FILE["tmp_name"];
        $name = basename($FILE["name"]);
        $this->_path = "$uploads_dir/$name";
        move_uploaded_file($tmp_name, $this->_path);
        $sizes_dir = getenv('HOME')."/"."uploads/sizes/$name";
        $ext = pathinfo($name, PATHINFO_EXTENSION);

        $this->_original = $uploads_dir.$name;

        $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        $sizes = require_once $root.'/protohome/config/image_sizes.php';

        if (!file_exists($sizes_dir)) {
            mkdir($sizes_dir, 0777, true);
        }

        foreach($sizes as $folder_name => &$folder){

            $this->_sizes[] = [$folder_name => []];

            if (!file_exists("$sizes_dir/$folder_name")) {
                mkdir("$sizes_dir/$folder_name", 0777, true);
            }

            foreach($folder as $device_name => &$device){

                $save_dir = "$sizes_dir/$folder_name/$device_name";

                $this->sizes[$folder_name] = [$device_name => $save_dir];

                $thumbnail = $this->_manager
                ->make($this->_original)
                ->resize($device[0], $device[1], function ($constraint) {

                    $constraint->aspectRatio();
                    $constraint->upsize();
                    
                })->save("$save_dir.$ext", 100);

            }

        }
    

    }

    public function getPath(){

        return $this->_path;

    }

}

?>