<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/protohome/core/Connect.php';

class User extends Connect {

    protected $_name;
    protected $_email;
    protected $_password;

    public function index(){

        return $this->_fpdo->from('users');

    }

    public function show($id){

        return $this->_fpdo->from('users', $id);

    }

    public function insert(){

        $user = [

            "name" => $this->_name,
            "email" => $this->_email,
            "password" => $this->_password

        ];

        $query = $this->_fpdo->insertInto('users', $user);

        return $query->execute();

    }

    public function update($id){

        $user = [

            "name" => $this->_name,
            "email" => $this->_email,
            "password" => $this->_password

        ];

        $query = $this->_fpdo->update('users', $user, $id);
        return $query->execute();

    }

    public function delete($id){

        $query = $this->_fpdo->deleteFrom('users', $id);

        return $query->execute();

    }

}

?>