<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once $root.'/protohome/core/Connect.php';

class Property extends Connect{

    protected $_title;
    protected $_body;
    protected $_country;
    protected $_img;

    public function __construct(){

        parent::__construct();

    }

    public function index(){

        return $this->_fpdo->from('properties');

    }

    public function show($id){

        return $this->_fpdo->from('properties', $id);

    }

    public function insert(){

        $property = [

            "title" => $this->_title,
            "body" => $this->_body,
            "img_path" => $this->_img->getPath(),
            "country" => $this->_country

        ];

        $query = $this->_fpdo->insertInto('properties', $property);

        return $query->execute();

    }

    public function update($id){

        $property = [

            "title" => $this->_title,
            "body" => $this->_body,
            "image" => $this->_image,
            "country" => $this->_country

        ];

        $query = $this->_fpdo->update('properties', $property, $id);
        return $query->execute();

    }

    public function delete($id){

        $query = $this->_fpdo->deleteFrom('properties', $id);

        return $query->execute();

    }

}

?>