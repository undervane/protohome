
var reference;

$(function(){

    $('.dropify').dropify();

    CKEDITOR.replace( "editor" );

    $.mockjax({
        url: '/restful/api',
        responseTime : 6200,
        status: 200,
        responseText: {
            hello: "World!"
        } 
    });

    $(document).on('change', '#drop-img', function(e){

        var tgt = e.target || window.event.srcElement,
        files = tgt.files;

        // Parse image

        var reader = new FileReader();

        reader.onload = function () {
            $('#image').attr('src', reader.result);
        };

        reader.readAsDataURL(files[0]);

    });

    $(document).on('click', '.submit', function(){

        let data = $(this).attr('data');

        let title = $('#title').val();

        let editor = CKEDITOR.instances.text.getData();

        if(title != '' && editor != ''){

        switch (data){

            case "left": 

                $('#left-title').html(title);
                $('#left-div').html(editor);

                break;

            case "right": 

                $('#right-title').html(title);
                $('#right-div').html(editor);

                break;

            default: console.warn(`No case ${data}`);

        }

        $('#title').val('');
        CKEDITOR.instances.text.setData(''); 

    } else { alert('You have to complete all fields'); }

    });

    $(document).on('submit', '#form', function(e){

        e.preventDefault();

    });

    $(document).on('click', '#modal', function(e){

        if ($(e.target).is('#modal') && $(this).hasClass('hide') != true){

            // If modal is visible

            $(this).addClass('hide');
            $('#img-modal').addClass('hide');
            $('#editor-modal').addClass('hide');

            if (reference != undefined){

                let text = CKEDITOR.instances.text.getData();

                console.log(text);

                if (text.length != 0){

                    reference.html(text);

                } else {

                    reference.html('Untitled Text');

                }

            }

            updateData();

        }

    });

    $(document).on('click', '#image', function(){

        $('#modal').removeClass('hide');
        $('#img-modal').removeClass('hide');

    });

    $(document).on('keydown', 'input', function(){

        let length = $(this).val().length;

        $(this).attr('size', length);

        if (length == 0){

            $(this).attr('size', 7);

        }


    });

    $(document).on('focusout', 'input', function(){

        let length = $(this).val().length;

        $(this).attr('size', length);

        if (length == 0){

            $(this).attr('size', 7);
            $(this).val(`${$(this).attr('data')} Title`);

        }

        updateData();

    });

    $(document).on('click', '.text-div', function(e){

        $('#modal').removeClass('hide');
        $('#editor-modal').removeClass('hide');

        CKEDITOR.instances.text.setData($(this).html());

        reference = $(this);

    });

});

function updateData(){

    let formData = new FormData();
    
    formData.append("Image", $('#image'));
    formData.append("Left Title", $('input[left-title]'));
    formData.append("Left Content", $('div[data=left]').html());
    formData.append("Right Title", $('input[right-title]'));
    formData.append("Right Content", $('div[data=right]').html());

    $.ajax({

        url : '/restful/api',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache : false,
        processData: false,

        beforeSend: function(){

            $('#status').html('Saving...');

        },
        
        success: function(response) {

            $('#status').html('Saved');

            setTimeout(function(){

                $('#status').html('')

            }, 1500);

        },

        error: function() {

            $('#status').html('Error while saving');

        }

    });

}


