<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title></title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
      <link rel="stylesheet" href="css/main.css">

      <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
      <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

  </head>

  <body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <p id="status"></p>

    <div id="modal" class="hide">

      <div id="img-modal" class="hide">

        <div class="drop-container">
          <label for="img">Upload your image</label>
          <input type="file" class="dropify" id="drop-img" name="img" data-height="300"/>
        </div>

      </div>

      <div id="editor-modal" class="hide">

        <div class="editor-container">
            <textarea id="text" name="editor"></textarea>
        </div>

      </div>

    </div>
    
    <div class="container-fluid">

    <form>

      <div class="img-container">
        <img id="image" src="img/placeholder.png"/>
      </div>

      <div class="articles-container row">

        <div class="offset-md-4 col-md-4 text-center">

          <input type="text" name="left-title" data="Left" size="7" value="Left Title" maxlength="20" />
          <div class="text-div" data="left">

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In schola desinis. Duo Reges: constructio interrete. Summae mihi videtur inscitiae. </p>
            
            <p>Disserendi artem nullam habuit. <b>Atqui reperies, inquit, in hoc quidem pertinacem;</b> Gerendus est mos, modo recte sentiat. Sed residamus, inquit, si placet. </p>
            
            </div>

        </div>

      </div>

    </form>

    </div>

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mockjax/1.6.2/jquery.mockjax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

  </body>
</html>
